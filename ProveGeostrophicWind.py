# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 16:21:25 2020

Module: Introduction to Numerical Modelling
        MTMW12

Student ID: lc815154

Description: This script contains the code to prove the order of accuracy
            of the method using 3 points.
            
"""
# Essential libraries and files imported to run the code.

import numpy as np
from Equations import *
import Numer_diff as diff
import matplotlib.pyplot as plt
from GeostrophicWind import u, u_ex, p, incry, y, error


N_n = 10000 # Incremented number of steps

incry_n = (properties["ymax"] - properties["ymin"])/N_n # Increment value using
                                                    # the values of ymax
                                                    # and ymin from the
                                                    # dicionary of "properties"


y_n = np.linspace(properties["ymin"], properties["ymax"], N_n+1) # Values of y
                                                                # for N_n
                                                                # intervals                                                                
p_n = pressure(y_n) # The pressure for N_n intervals
u_n = GeosU(diff.gradient(p_n, incry_n)) # Geostrophic wind for N_n intervals
u_exn = GeosUexact(y_n) # For N_n ntervals

y_1 = np.where(y == 500000)[0][0] # Index where the value of y = 500000
y_2 = np.where(y_n == 500000)[0][0] # Index where the value of y_n = 500000
                                                       
e1 = error # Error of the geostrophic wind using N intervals 
e2 = np.abs(100*(u_n - u_exn)/u_n) # Error of the geostrophic wind for N_n intervals

# Below, the code to plot the error versus the resolution
# where it's plotted the value of the increments versus the value of the
# error where y and y_ n are equal to 500000

plt.figure("Order")
plt.plot([incry, incry_n], [e1[y_1], e2[y_2]], color = "brown") # Tendency line
plt.plot([incry, incry_n], [e1[y_1], e2[y_2]], "o", color = "forestgreen") 
plt.xlabel("$\Delta$y (m)")
plt.ylabel("Error $\epsilon$")
plt.xscale("log")
plt.yscale("log")
plt.grid()
plt.show()

# Equation that calculates the order of accuracy
n = (np.log(e2[y_2])-np.log(e1[y_1]))/(np.log(incry_n)-np.log(incry))
print("Order of accuracy: ",np.around(n,3), "\nError for N = 10: ",e1[y_1], \
      "\n Error for N = 10000: ", e2[y_2])