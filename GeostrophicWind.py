# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 10:35:23 2020

Module: Introduction to Numerical Modelling
        MTMW12

Student ID: lc815154

Description: This script contains the code to calculate the value of the 
            geostrophic wind using the Taylor series for 3 point and the
            exact value of the geostrophic wind.

"""
# Essential libraries and files imported to run the code.

import numpy as np
from Equations import *
import Numer_diff as diff
import matplotlib.pyplot as plt

N = 10 # Number of steps
incry = (properties["ymax"] - properties["ymin"])/N # Increment value using
                                                    # the values of ymax
                                                    # and ymin from the
                                                    # dicionary of "properties"


y = np.linspace(properties["ymin"], properties["ymax"], N+1) # Values of y

p = pressure(y) # Value of the pressure corresponding to the y positions

u = GeosU(diff.gradient(p, incry)) # Value of the geostrophic wind
u_ex = GeosUexact(y) # Exact value of the geostrophyic wind

error = np.abs(100*(u-u_ex)/u) # array containing the error between the 
                              # numerical geostrohpic wind and the
                              # analitical geostrophyic wind
                              # using 2 points
                              
# Below, the code so as to plot the numerical geostrophic wind and the
# analytical geostrophyic wind with respect to y.

plt.figure("Geostrophic wind")
plt.plot(y, u, label = "Numerical method 3 points", linewidth = 0.9)
plt.plot(y, u_ex, label = "Analytic method", linewidth = 0.9)
plt.xlabel("y (m)")
plt.ylabel("u (m·s-1)")
plt.legend()
plt.grid()
plt.xlim((y[0],y[-1]))
plt.show()

# Below, the code to plot the errors between the numerical and the analytical
# geostrophic wind.

plt.figure("Error")
plt.plot(y, error, label = "Numerical method 3 points", linewidth = 0.8)
plt.ylabel("Error %")
plt.xlabel("y (m)")
plt.grid()
plt.show()




