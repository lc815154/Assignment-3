# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 17:59:00 2020

@author: inesm
"""
# Libraries and scripts improted, essential to run the code

import numpy as np
from Equations import *
import Numer_diff as diff
import matplotlib.pyplot as plt
from GeostrophicWind import u, u_ex, p, incry, y, error

# --- 5 point method ---------------------------------------------------------

u5 = GeosU(diff.gradient2(p, incry)) # Value of the geostrophic wind 
                                     # using 5 points

error5 = np.abs(100*(u5-u_ex)/u5) # list containing the error between the 
                              # numerical geostrohpic wind and the
                              # analitical geostrophyic wind
                              # using 5 points


# Belor, the code to plot the value of the geostrophic wind using 5 points,
# 3 points and the exact value of the wind.
                              
plt.figure("Improved method 5 points")
plt.plot(y, u5, label = "5 points numerical method", linewidth = 0.8)
plt.plot(y, u, label = "3 points numerical method", linewidth = 0.8)
plt.plot(y, u_ex, label = "Exact value", linewidth = 0.8)
plt.ylabel("u (m·s-1)")
plt.xlabel("y (m)")
plt.legend()
plt.grid()
plt.show()

# Below, the code to plot the error of the geostrophic wind following the
# 5 point method and the 3 point method.

plt.figure("Errors improved method 5 points")
plt.plot(y, error5, label = "5 points numerical method", linewidth = 0.8)
plt.plot(y, error, label = "3 points numerical method", linewidth = 0.8)
plt.xlabel("y (m)")
plt.ylabel("$\epsilon$ %")
plt.legend()
plt.grid()
plt.show()

# --- Alternative improved method --------------------------------------------

u_imprv = GeosU(diff.gradient_impr(p, incry)) # Improved endpoints
error_imprv = np.abs(100*(u_imprv-u_ex)/u_imprv) # list containing the error between
                                          # the numerical geostrohpic wind and
                                          # the analitical geostrophyic wind
                                          # using an improved method

plt.figure("Improved method")
plt.plot(y, u_imprv, label = "Improved numerical method", linewidth = 0.8)
plt.plot(y, u, label = "3 points numerical method", linewidth = 0.8)
plt.plot(y, u_ex, label = "Exact value", linewidth = 0.8)
plt.ylabel("u (m·s-1)")
plt.xlabel("y (m)")
plt.legend()
plt.grid()
plt.show()

# Below, the code to plot the error of the geostrophic wind following the
# 5 point method and the 3 point method.

plt.figure("Errors improved")
plt.plot(y, error_imprv, label = "Improved numerical method", linewidth = 0.8)
plt.plot(y, error, label = "3 points numerical method", linewidth = 0.8)
plt.xlabel("y (m)")
plt.ylabel("$\epsilon$ %")
plt.legend()
plt.grid()
plt.show()