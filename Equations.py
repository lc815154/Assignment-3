# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 08:59:33 2020

Module: Introduction to Numerical Modelling
        MTMW12

Student ID: lc815154

Description: This script contains the functions of the pressure, the
            geostrophic wind using the derivative from Taylor's expansion
            and the exact value of the geostrophic wind.
            
"""

import numpy as np

properties = {'pa' : 1e5,
              'pb' : 200,
              'f' : 1e-4,
              'rho' : 1,
              'L' : 2.4e6,
              'ymin' : 0,
              'ymax' : 1e6}

def pressure(y):
    
    """This function returns the value of the pressure for a given position y,
        for fixed values of pa, pb and L using the dictionary "properties"."""
        
    pa = properties["pa"] # Pressure Pa
    pb = properties["pb"]
    L = properties["L"]
    
    p = pa + pb*np.cos(y*np.pi/L) # 
    
    return p

def GeosU(diffp):
    
    """This function returns the value of the geostrophic wind using the
        approximate numerical value of the gradient of the pressure, for a
        given density and f parameter using the dictionary "properties"."""
        
    rho = properties["rho"]
    f = properties["f"]
        
    u = -diffp/(rho*f)
        
    return u

def GeosUexact(y):
    
    """This function retuns the value of the geostrophic wind using the exact
    differentiation of the pressure for a given position y, and fixed 
    parameters from the dictionary "properties"."""
    
    rho = properties["rho"]
    f = properties["f"]
    pb = properties["pb"]
    L = properties["L"]
    
    uex = pb*np.pi*np.sin(y*np.pi/L)/(rho*f*L)
    
    return uex
    
