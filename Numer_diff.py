# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 10:11:09 2020

Module: Introduction to Numerical Modelling
        MTMW12
        
Student ID: lc815154
        
Description: This script contains the functions that computes the derivatives
            of the pressure for 3 points, 5 points and an improved method.
            
"""

import numpy as np
from Equations import *

# Numerical methods

def gradient(p, incry):
    
    """This function returns the gradient of the pressure p using 3 points, 
    from ymin to ymax and an increment incry."""
    
    dpdy = np.empty(len(p))
    dpdy[0] = (p[1] - p[0])/incry
    dpdy[-1] = (p[-1] - p[-2])/incry
    
    for i in range(1,len(p)-1):
        dpdy[i] = (p[i+1] - p[i-1])/(2*incry)
        
    return dpdy

def gradient2(p, incry):
    
    """This function returns the gradient of the pressure using 5 points,
    from ymin to ymax and an increment incry."""
    
    dpdy = np.empty(len(p))
    
    dpdy[0] = (p[1] - p[0])/incry
    dpdy[1] = (p[2] - p[1])/incry
    
    dpdy[-2] = (p[-2] - p[-3])/incry
    dpdy[-1] = (p[-1] - p[-2])/incry
    
    for i in range(2,len(p)-2):
        dpdy[i] = (-p[i+2] + 8*p[i+1] - 8*p[i-1] + p[i-2])/(12*incry)
        
    return dpdy

def gradient_impr(p, incry):
    
    """This function returns the gradient of the pressure p using 5 points, 
    from ymin to ymax and an increment incry, being an alternative version
    of gradient2."""
    
    dpdy = np.empty(len(p))

    dpdy[0] = (p[2] + p[1] - 2*p[0])/(3*incry)
    dpdy[1] = (p[3] + p[2] - 2*p[1])/(3*incry)
    
    dpdy[-1] = (2*p[-1] - p[-2] - p[-3])/(3*incry)
    dpdy[-2] = (2*p[-2] - p[-3] - p[-4])/(3*incry)

    
    for i in range(2,len(p)-2):
        dpdy[i] = (-p[i+2] + 4*p[i+1]-3*p[i])/(2*incry)
        
    return dpdy