To reproduce the results given in Assign2Report_lc815154, RUN the following
files:

1. "Equations.py": This file contains the equations of the geostrophic wind
    using the derivative from Taylor's expansion, the exact value of the 
    geostrophic wind with the exact derivate and the physical properties
    to calculate the value of the wind.
    
2. "Numer_diff.py": This file contains the 3 point, 5 point and an improved
    Taylor's method to calculate the value of the Geostrophic wind.
    
3. "GeostrophicWind.py": This file contains the code to reproduce the value
    of the geostrophic wind, the exact value of the geostrophic wind and the
    associated error.
    
4. "ProveGeostrophicWind.py": This file containes the code to prove that
    the implemented methods to calculate the geostrophic wind are working
    as expected.

5. "Improved.py": This file contains the code to execute the alternative
    improved methods to calculate the geostrophic wind, by using 5 points
    and an alternative version to use 5 points.